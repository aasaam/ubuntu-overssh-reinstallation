#!/bin/bash

UBUNTU_VERSION=$(whiptail --title "$PROG_TITLE"  --radiolist \
 "Select version of ubuntu want to download/install" $SIZE_X $SIZE_Y 4 \
 "UBUNTUISO_FOCAL" "Ubuntu 20.04 focal (LTS)" ON \
 "UBUNTUISO_BIONIC" "Ubuntu 18.04 bionic (LTS)" OFF \
 "UBUNTUISO_XENIAL" "Ubuntu 16.04 xenial (LTS)" OFF \
 "SKIP" "Skip download iso" OFF \
3>&1 1>&2 2>&3)

ISO_URL_TO_DOWNLOAD=''
if [ "$UBUNTU_VERSION" == "UBUNTUISO_FOCAL" ]; then
  ISO_URL_TO_DOWNLOAD=$UBUNTUISO_FOCAL
elif [ "$UBUNTU_VERSION" == "UBUNTUISO_BIONIC" ]; then
  ISO_URL_TO_DOWNLOAD=$UBUNTUISO_BIONIC
elif [ "$UBUNTU_VERSION" == "UBUNTUISO_XENIAL" ]; then
  ISO_URL_TO_DOWNLOAD=$UBUNTUISO_XENIAL
elif [ "$UBUNTU_VERSION" != "SKIP" ]; then
  exit;
fi

if [ ! -z "$ISO_DOWNLOAD_PROXY" ]; then
  export http_proxy=$ISO_DOWNLOAD_PROXY
  export https_proxy=$ISO_DOWNLOAD_PROXY
fi

echo -e "\033[0;31mDownloading $ISO_URL_TO_DOWNLOAD...!\033[0m"
curl -o $PROJECTPATH/tmp/mini.iso -sL -O -C - $ISO_URL_TO_DOWNLOAD
echo "Done"
