<?php
define('PRESEED_PATTERN', '/^[a-z0-9]{32}$/i');
define('TMP_DIR', sys_get_temp_dir());
define('TMP_FILE_PATTERN', 'preseed_server_%s.bin');

function gc_olds() {
  foreach (glob(TMP_DIR . DIRECTORY_SEPARATOR . str_replace('%s', '*', TMP_FILE_PATTERN)) as $file) {
    if(time() - filemtime($file) > 14400) {
      unlink($file);
    }
  }
}

function setStatus($code) {
  $http_status_codes = [
    201 => 'Created',
    400 => 'Bad Request',
    403 => 'Forbidden',
    404 => 'Not Found',
    500 => 'Internal Server Error',
    502 => 'Bad Gateway',
  ];
  if (isset($http_status_codes[$code])) {
    $error = sprintf('%s %s', $code, $http_status_codes[$code]);
  } else {
    $error = sprintf('%s %s', 500, $http_status_codes[500]);
  }
  header('HTTP/1.1 ' . $error);
  if ($code !== 201) {
    header('Content-Type: text/plain');
    echo $error;
  }
  exit;
}

gc_olds();

if (isset($_POST['preseed']) && is_string($_POST['preseed']) && isset($_FILES["preseed"])) {
  if (!preg_match(PRESEED_PATTERN, $_POST['preseed'])) {
    setStatus(400);
  }
  $desFile = vsprintf('%s%s%s', [
    TMP_DIR,
    DIRECTORY_SEPARATOR,
    sprintf(TMP_FILE_PATTERN, $_POST['preseed']),
  ]);
  $fileData = file_get_contents($_FILES["preseed"]["tmp_name"]);
  file_put_contents($desFile, $fileData);
  setStatus(201);
}

if (isset($_GET['preseed']) && is_string($_GET['preseed'])) {
  if (!preg_match(PRESEED_PATTERN, $_GET['preseed'])) {
    setStatus(400);
  }
  $sourceFile = vsprintf('%s%s%s', [
    TMP_DIR,
    DIRECTORY_SEPARATOR,
    sprintf(TMP_FILE_PATTERN, $_GET['preseed']),
  ]);
  if (file_exists($sourceFile)) {
    header('Content-Type: text/plain');
    header('Content-Length: ' . filesize($sourceFile));
    readfile($sourceFile);
    exit;
  } else {
    setStatus(404);
  }
}

if (isset($_GET['ping-preseed'])) {
  setStatus(201);
}

setStatus(403);
